package com.myapi.apiexample.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.myapi.apiexample.databinding.ItemFavBinding
import com.myapi.apiexample.model.Favorites

/**
 * We don't need to use paging for this adapter, because we are getting list from local db.
 * */
class FavAdapter(private val onItemClick: (Favorites) -> Unit) :
    RecyclerView.Adapter<FavAdapter.ViewHolder>() {

    private var favList = ArrayList<Favorites>()

    class ViewHolder(val binding: ItemFavBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(fav: Favorites) {
            binding.fav = fav
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemFavBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val fav = favList[position]
        holder.bind(fav)
        holder.binding.rootLay.setOnClickListener {
            onItemClick(fav)
        }
    }

    override fun getItemCount() = favList.size

    fun setData(list: List<Favorites>) {
        favList = list as ArrayList<Favorites>
        notifyDataSetChanged()
    }
}