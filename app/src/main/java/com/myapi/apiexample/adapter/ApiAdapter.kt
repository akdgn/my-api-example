package com.myapi.apiexample.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.myapi.apiexample.R
import com.myapi.apiexample.databinding.ItemListBinding
import com.myapi.apiexample.model.IPhoto

/**
 * check items for paging
 */
private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<IPhoto>() {
    override fun areItemsTheSame(oldItem: IPhoto, newItem: IPhoto): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: IPhoto, newItem: IPhoto): Boolean =
        oldItem.url == newItem.url
}

/**
 * Used data binding
 * it provides clean code, binding data with row item very easy
 * */
class ApiAdapter(
    private val onItemClick: (IPhoto?) -> Unit,
    private val onDelete: (IPhoto?) -> Unit,
    private val onFav: (IPhoto?) -> Unit
) : PagingDataAdapter<IPhoto, ApiAdapter.ViewHolder>(REPO_COMPARATOR) {

    class ViewHolder(val binding: ItemListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(iPhoto: IPhoto?) {
            binding.photo = iPhoto
            binding.executePendingBindings()
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val iPhoto = getItem(position)
        holder.bind(iPhoto)
        holder.binding.rootLay.setOnClickListener {
            onItemClick(iPhoto)
        }
        holder.binding.btnDelete.setOnClickListener {
            onDelete(iPhoto)
        }
        holder.binding.btnFav.setOnClickListener {
            iPhoto?.isFav = !iPhoto?.isFav!!
            holder.binding.btnFav.setImageResource(if (iPhoto.isFav) R.drawable.ic_baseline_favorite_24 else R.drawable.ic_baseline_favorite_border_24)
            onFav(iPhoto)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }
}