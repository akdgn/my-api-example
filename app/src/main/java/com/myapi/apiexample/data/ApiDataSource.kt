package com.myapi.apiexample.data

import android.util.Log
import androidx.paging.PagingState
import androidx.paging.rxjava3.RxPagingSource
import com.myapi.apiexample.model.IPhoto
import com.myapi.apiexample.repo.FakeRepo
import io.reactivex.rxjava3.core.Single

/**
 * This class is so important to paging data truly.
 * Calculating the page for the api, the start and end values should behave like pages
 * */
class ApiDataSource(private val repo: FakeRepo) : RxPagingSource<Int, IPhoto>() {

    companion object {
        val ITEM_COUNT_PER_PAGE = 50
    }

    override fun getRefreshKey(state: PagingState<Int, IPhoto>): Int? {
        return null
    }

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, IPhoto>> {
        return Single.create { emitter ->
            var page: Int? = params.key
            if (page == null)
                page = 0

            val start = page * ITEM_COUNT_PER_PAGE // 0 => 0 1--50 2--100...
            val end = start + ITEM_COUNT_PER_PAGE // 0 => 50 1--100 2--150...

            val prevPage = if (page == 0) null else page - 1
            val nextPage = page + 1
            repo.getPhotos(start, end, object : FakeRepo.ResultListener {
                override fun onLoadedList(list: List<IPhoto>) {
                    emitter.onSuccess(
                        LoadResult.Page(
                            list,
                            prevPage,
                            if (list.isEmpty()) null else nextPage
                        )
                    )
                    Log.d(FakeRepo.TAG, "onLoadedList: list.size : ${list.size}")
                }
            })

        }
    }
}