package com.myapi.apiexample.util

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * async task is deprecated, so coroutine is useful for bg thread operations
 * */
abstract class CoroutineAsyncTask<Params, Progress, Result> {

    abstract fun doInBackground(vararg params: Params): Result

    open fun onProgressUpdated(vararg values: Progress?) {}

    open fun onPostExecute(result: Result?) {}

    protected fun publishProgress(vararg progress: Progress?) {
        GlobalScope.launch(Dispatchers.Main) {
            onProgressUpdated(*progress)
        }
    }

    fun execute(vararg params: Params) {
        GlobalScope.launch(Dispatchers.Default) {
            val result = doInBackground(*params)
            withContext(Dispatchers.Main) {
                onPostExecute(result)
            }
        }
    }

}