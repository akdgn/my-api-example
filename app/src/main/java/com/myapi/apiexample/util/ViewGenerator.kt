package com.myapi.apiexample.util

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

/**
 * data binding generator
 * img and text of some views should be customized
 * */

object ViewGenerator {

    const val TYPE_ID = "id : "
    const val TYPE_ALBUM_ID = "album_id : "

    @JvmStatic
    @BindingAdapter("loadRemoteImg")
    fun ImageView.loadRemoteImg(url: String?) {
        url?.let {
            Picasso.get()
                .load(it)
                .into(this)
        }

    }

    @JvmStatic
    @BindingAdapter("customType", "customId")
    fun TextView.customText(which: String, id: String) { // etc: id : 1 albumId : 1
        val formattedText = "$which$id"
        this.text = formattedText
    }

}