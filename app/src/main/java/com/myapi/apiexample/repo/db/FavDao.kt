package com.myapi.apiexample.repo.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.myapi.apiexample.model.Favorites

@Dao
interface FavDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg fav: Favorites)

    @Query("DELETE FROM favorites WHERE photoId = :photoId")
    fun delete(photoId: Int)

    @Update
    fun update(vararg fav: Favorites)

    @Query("SELECT * FROM favorites")
    fun getFavList(): LiveData<List<Favorites>>

    @Query("SELECT isFav=1 FROM favorites WHERE id = :id")
    fun isFav(id: Int): Boolean

    @Query("SELECT isFav=1 FROM favorites WHERE title = :title and thumb = :thumb")
    fun isFav(title: String, thumb: String): Boolean

    @Query("SELECT EXISTS(SELECT * FROM favorites WHERE photoId = :photoId)")
    fun exist(photoId: Int): Boolean


}