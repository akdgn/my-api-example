package com.myapi.apiexample.repo.db

import android.content.Context
import com.myapi.apiexample.model.Favorites
import com.myapi.apiexample.util.CoroutineAsyncTask
/**
 * DbRepo provides us that we can reach all bg thread db issues.
 * Inserted the item if the item is not existed in db
 * This will provide us to detect fav items in the main api list
 * */
class DbRepo(context: Context) {

    val database = FavDatabase.getInstance(context)
    val dao = database.favDao()
    val favs = dao.getFavList()

    companion object {
        fun instance(context: Context) = DbRepo(context)
    }

    fun insert(fav: Favorites, onDone: (Boolean) -> Unit) {
        InsertTask(onDone).execute(fav)
    }

    inner class InsertTask(var onDone: (Boolean) -> Unit) :
        CoroutineAsyncTask<Favorites, Boolean, Boolean>() {
        override fun doInBackground(vararg params: Favorites): Boolean {
            return if (!dao.exist(params[0].photoId)) {
                dao.insert(params[0])
                true
            } else {
                dao.delete(params[0].photoId)
                false
            }
        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)
            result?.let {
                onDone(it)
            }
        }
    }

    inner class UpdateTask(var onDone: (Boolean) -> Unit) :
        CoroutineAsyncTask<Favorites, Boolean, Boolean>() {
        override fun doInBackground(vararg params: Favorites): Boolean {
            if (dao.exist(params[0].photoId)) {
                dao.update(params[0])
                return true
            }
            return false
        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)
            result?.let {
                onDone(it)
            }
        }
    }
}