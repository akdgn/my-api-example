package com.myapi.apiexample.repo.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.myapi.apiexample.model.Favorites
/**
 * fallbackToDestructiveMigration = if table changes we are ignoring history. that means all history will be removed when update the application
 * if we dont want this, we can add migration when table changed.
 */
@Database(entities = [Favorites::class], version = 1, exportSchema = false)
abstract class FavDatabase : RoomDatabase() {

    abstract fun favDao(): FavDao

    companion object {
        val database: FavDatabase? = null

        @Synchronized
        fun getInstance(context: Context): FavDatabase {
            database?.let {
                return it
            } ?: return Room.databaseBuilder(
                context.applicationContext,
                FavDatabase::class.java,
                "Fav_Database"
            ).fallbackToDestructiveMigration().build()
        }
    }
}