package com.myapi.apiexample.repo

import android.content.Context
import android.util.Log
import com.myapi.apiexample.model.IPhoto
import com.myapi.apiexample.repo.db.DbRepo
import com.myapi.apiexample.service.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
/**
 * This repo provides to return api datas.
 * by using this class, we can fetch our data in view model. (MVVM)
 * **/
class FakeRepo(private val context: Context) {

    private val dbRepo = DbRepo.instance(context)

    companion object {
        const val TAG = "FakeRepo_"

        @JvmStatic
        fun instance(context: Context) = FakeRepo(context)
    }

    /**
     * delete item from api
     * */
    fun deletePhoto(iPhoto: IPhoto, onResponse: (Boolean, IPhoto?) -> Unit) {
        val apiService = RetrofitClient.instance(context).mApi.deletePost(iPhoto.id ?: 0)
        apiService.enqueue(object : Callback<IPhoto> {
            override fun onResponse(call: Call<IPhoto>, response: Response<IPhoto>) {
                onResponse(response.isSuccessful, response.body())
                Log.d(TAG, "onResponse: ${response.code()}")
            }

            override fun onFailure(call: Call<IPhoto>, t: Throwable) {
                onResponse(false, null)
            }

        })
    }

    /**
     * post item to api
     * */
    fun postPhoto(iPhoto: IPhoto, onResponse: (Boolean, IPhoto?) -> Unit) {
        val apiService = RetrofitClient.instance(context).mApi.postPhoto(iPhoto)
        apiService.enqueue(object : Callback<IPhoto> {
            override fun onResponse(call: Call<IPhoto>, response: Response<IPhoto>) {
                onResponse(response.isSuccessful, response.body())
            }

            override fun onFailure(call: Call<IPhoto>, t: Throwable) {
                onResponse(false, null)
            }

        })
    }

    /**
     * get single photo from api
     * */
    fun getPhoto(id: Int, onPhotoResult: (IPhoto?) -> Unit) {
        val apiService = RetrofitClient.instance(context).mApi.getPhoto(id)
        apiService.enqueue(object : Callback<IPhoto> {
            override fun onResponse(call: Call<IPhoto>, response: Response<IPhoto>) {
                if (response.isSuccessful) {
                    onPhotoResult(response.body())
                } else {
                    Log.e(TAG, "onResponse: ${response.code()}")
                }
            }

            override fun onFailure(call: Call<IPhoto>, t: Throwable) {
                Log.e(TAG, "onFailure: $t")
                onPhotoResult(null)
            }
        })
    }

    /**
     * get all photos from api, there are 5000 photos, we are getting 50 photos for each request
     * to improve performance of this call, it can be decreased photo counts for each request.
     * */
    fun getPhotos(start: Int, end: Int, listener: ResultListener?) {
        val apiService = RetrofitClient.instance(context).mApi.getPhotos(start, end)
        apiService.enqueue(object : Callback<ArrayList<IPhoto>> {
            override fun onResponse(
                call: Call<ArrayList<IPhoto>>,
                response: Response<ArrayList<IPhoto>>
            ) {
                if (response.isSuccessful) {
                    val mList = response.body()
                    val finalList = ArrayList<IPhoto>()

                    Thread {
                        mList?.forEach { photo ->
                            val exist =
                                dbRepo.dao.exist(photo.id ?: 0)
                            val p = IPhoto(
                                photo.albumId,
                                photo.id,
                                photo.title,
                                photo.url,
                                photo.thumbnailUrl,
                                exist
                            )
                            finalList.add(p)
                        }
                        listener?.onLoadedList(finalList)
                    }.start()

                }
            }

            override fun onFailure(call: Call<ArrayList<IPhoto>>, t: Throwable) {
                Log.e(TAG, "onResponse: ${t.message}")
                listener?.onLoadedList(ArrayList())
                t.printStackTrace()
            }

        })
    }

    /**
     * After getting response it will be filled to list
     * */
    interface ResultListener {
        fun onLoadedList(list: List<IPhoto>)
    }

}