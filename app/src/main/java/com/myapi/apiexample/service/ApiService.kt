package com.myapi.apiexample.service

import com.myapi.apiexample.model.IPhoto
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @GET("photos")
    fun getPhotos(
        @Query("_start") start: Int,
        @Query("_end") end: Int,
    ): Call<ArrayList<IPhoto>>

    @GET("photos/{id}")
    fun getPhoto(@Path("id") id: Int): Call<IPhoto>

    @POST("photos")
    fun postPhoto(@Body iPhoto: IPhoto): Call<IPhoto>

    @DELETE("post/{id}")
    fun deletePost(@Path("id") id: Int): Call<IPhoto>
}