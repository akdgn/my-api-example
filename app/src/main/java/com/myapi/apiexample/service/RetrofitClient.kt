package com.myapi.apiexample.service

import android.content.Context
import com.myapi.apiexample.util.Utils.hasNetwork
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * This class provides retrofit client
 * offline cache size may be changed
 * this client class provides offline caching when data is loaded first time
 * */
class RetrofitClient(private val context: Context) {

    val cacheSize = (5 * 1024 * 1024).toLong()

    companion object {
        private var BASE_URL = "https://jsonplaceholder.typicode.com/" // public api base url

        fun instance(context: Context) = RetrofitClient(context)
    }

    val mApi: ApiService by lazy {
        val myCache = Cache(context.cacheDir, cacheSize)
        val okHttpClient = OkHttpClient.Builder()
            .cache(myCache)
            .addInterceptor { chain ->
                var request = chain.request()
                request = if (hasNetwork(context))
                    request.newBuilder().header("Cache-Control", "public, max-age=" + 5).build()
                else
                    request.newBuilder().header(
                        "Cache-Control",
                        "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7
                    ).build()
                chain.proceed(request)
            }
            .build()
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .build().create(ApiService::class.java)
    }
}