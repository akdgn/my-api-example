package com.myapi.apiexample.model

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * favorite db table
 * */
@Entity(tableName = "favorites")
data class Favorites(
    val url: String?,
    val thumb: String?,
    val albumId: Int,
    val photoId: Int,
    val title: String?,
    val isFav: Boolean?,
    @PrimaryKey(autoGenerate = true) val id: Int = 0
)