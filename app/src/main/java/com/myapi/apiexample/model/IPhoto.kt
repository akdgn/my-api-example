package com.myapi.apiexample.model

/**
 * api model class
 * */
data class IPhoto(
    var albumId: Int?,
    var id: Int? = 0,
    var title: String?,
    var url: String?,
    var thumbnailUrl: String?,
    var isFav: Boolean = false
)