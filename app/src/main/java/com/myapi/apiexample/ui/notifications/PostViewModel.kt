package com.myapi.apiexample.ui.notifications

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.myapi.apiexample.R
import com.myapi.apiexample.model.IPhoto
import com.myapi.apiexample.repo.FakeRepo

class PostViewModel(private val app: Application) : AndroidViewModel(app) {

    private val fakeRepo = FakeRepo.instance(app.applicationContext)

    private val _iPhoto = MutableLiveData<IPhoto>()
    val iPhoto get() = _iPhoto

    fun postPhoto(iPhoto: IPhoto) {
        fakeRepo.postPhoto(iPhoto, onResponse = { success, photo ->
            val msg =
                if (success) app.applicationContext.getString(R.string.post_success) else app.applicationContext.getString(
                    R.string.post_error
                )
            _iPhoto.value = photo
            Toast.makeText(
                app.applicationContext,
                msg,
                Toast.LENGTH_SHORT
            ).show()
        })
    }

}