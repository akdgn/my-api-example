package com.myapi.apiexample.ui.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.myapi.apiexample.adapter.FavAdapter
import com.myapi.apiexample.databinding.FragmentFavoriteBinding

class FavoriteFragment : Fragment() {

    private var _binding: FragmentFavoriteBinding? = null
    private val binding get() = _binding!!
    private lateinit var vm: FavoriteViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        vm = ViewModelProvider(this).get(FavoriteViewModel::class.java)
        _binding = FragmentFavoriteBinding.inflate(inflater, container, false)
        val adapter = FavAdapter(onItemClick = { fav ->
            val builder = StringBuilder()
                .append("Id : ${fav.id}")
                .append("\n")
                .append("album id : ${fav.albumId}")
                .append("\n")
                .append("title : ${fav.title}")

            Toast.makeText(activity, builder.toString(), Toast.LENGTH_SHORT).show()
        })
        vm.favList.observe(viewLifecycleOwner, {
            adapter.setData(it)
            binding.txtEmptyList.visibility = if (it.isEmpty()) View.VISIBLE else View.GONE
        })
        binding.rvFav.adapter = adapter
        return binding.root
    }
}