package com.myapi.apiexample.ui.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.myapi.apiexample.databinding.FragmentPostBinding
import com.myapi.apiexample.model.IPhoto

class PostFragment : Fragment() {

    private lateinit var postViewModel: PostViewModel
    private var _binding: FragmentPostBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        postViewModel = ViewModelProvider(this).get(PostViewModel::class.java)
        _binding = FragmentPostBinding.inflate(inflater, container, false)
        binding.apply {
            lifecycleOwner = this@PostFragment
            fragment = this@PostFragment
            viewModel = postViewModel
        }
        return binding.root
    }

    fun postData() {
        val photo = IPhoto(
            binding.albumId.text.toString().toInt(),
            title = binding.edtTitle.text.toString(),
            thumbnailUrl = binding.edtthumb.text.toString(),
            url = binding.edtUrl.text.toString()
        )
        postViewModel.postPhoto(photo)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}