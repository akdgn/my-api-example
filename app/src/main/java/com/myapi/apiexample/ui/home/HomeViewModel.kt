package com.myapi.apiexample.ui.home

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import com.myapi.apiexample.R
import com.myapi.apiexample.data.ApiDataSource
import com.myapi.apiexample.data.ApiDataSource.Companion.ITEM_COUNT_PER_PAGE
import com.myapi.apiexample.model.Favorites
import com.myapi.apiexample.model.IPhoto
import com.myapi.apiexample.repo.FakeRepo
import com.myapi.apiexample.repo.FakeRepo.Companion.TAG
import com.myapi.apiexample.repo.db.DbRepo


class HomeViewModel(private val app: Application) : AndroidViewModel(app) {

    val fakeRepo = FakeRepo.instance(app.applicationContext)

    private val _progressVis = MutableLiveData(false)
    val progressVis get() = _progressVis
    private val dbRepo = DbRepo.instance(app.applicationContext)

    fun deletePhoto(iPhoto: IPhoto) {
        fakeRepo.deletePhoto(iPhoto, onResponse = { success, photo ->
            Log.d(TAG, "deletePhoto: photo.url : ${photo?.url}")
            Log.d(TAG, "deletePhoto: photo.thumbnailUrl : ${photo?.thumbnailUrl}")
            Log.d(TAG, "deletePhoto: photo.id : ${photo?.id}")
            Log.d(TAG, "deletePhoto: photo.albumId : ${photo?.albumId}")
            if (success)
                Toast.makeText(app.applicationContext, "Delete Success", Toast.LENGTH_SHORT).show()
        })
    }

    fun pagingData(): LiveData<PagingData<IPhoto>> {
        val pager: Pager<Int, IPhoto> = getPager()
        return pager.liveData.cachedIn(this.viewModelScope)
    }

    fun getPager(): Pager<Int, IPhoto> {
        val prefetchDistance = 6
        val pagingConfig = PagingConfig(
            ITEM_COUNT_PER_PAGE,
            prefetchDistance,
            false,
            ITEM_COUNT_PER_PAGE,
            Int.MAX_VALUE,
            Int.MIN_VALUE
        )
        return Pager(pagingConfig, null, {
            ApiDataSource(fakeRepo)
        })
    }

    fun insertOrDelete(fav: Favorites) {
        dbRepo.insert(fav, onDone = { inserted ->
            val text =
                if (inserted) app.applicationContext.getString(R.string.txt_added_fav) else app.applicationContext.getString(
                    R.string.txt_deleted
                )

            Toast.makeText(
                app.applicationContext,
                text,
                Toast.LENGTH_SHORT
            ).show()
        })
    }
}