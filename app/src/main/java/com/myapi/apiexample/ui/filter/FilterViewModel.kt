package com.myapi.apiexample.ui.filter

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.myapi.apiexample.repo.FakeRepo

class FilterViewModel(private val app: Application) : AndroidViewModel(app) {

    private val fakeRepo = FakeRepo.instance(app.applicationContext)

    private val _thumb = MutableLiveData<String>()
    val thumb get() = _thumb

    private val _title = MutableLiveData<String>()
    val title get() = _title

    private val _img = MutableLiveData<String>()
    val img get() = _img

    private val _id = MutableLiveData<Int>()
    val id get() = _id

    private val _albumId = MutableLiveData<Int>()
    val albumId get() = _albumId

    private val _progressVis = MutableLiveData(false)
    val progressVis get() = _progressVis

    fun loadPhoto(idKey: Int) {
        _progressVis.value = true
        fakeRepo.getPhoto(idKey, onPhotoResult = {
            it?.let {
                id.value = it.id ?: -1
                albumId.value = it.albumId ?: -1
                img.value = it.url
                title.value = it.title
                thumb.value = it.thumbnailUrl
                _progressVis.value = false
            }
        })
    }
}