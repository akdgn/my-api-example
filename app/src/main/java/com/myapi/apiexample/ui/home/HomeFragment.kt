package com.myapi.apiexample.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.myapi.apiexample.adapter.ApiAdapter
import com.myapi.apiexample.databinding.FragmentHomeBinding
import com.myapi.apiexample.model.Favorites
import com.myapi.apiexample.util.Utils
import com.myapi.apiexample.util.ViewGenerator

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        binding.apply {
            lifecycleOwner = this@HomeFragment
        }
        val adapter = ApiAdapter(onItemClick = {
            if (it == null)
                return@ApiAdapter

            val itemClickText = StringBuilder()
                .append(ViewGenerator.TYPE_ALBUM_ID)
                .append(it.albumId)
                .append("\n")
                .append(ViewGenerator.TYPE_ID)
                .append(it.id).toString()

            Utils.toast(activity, itemClickText)
        }, onDelete = { photo ->
            photo?.let {
                homeViewModel.deletePhoto(it)
            }
        }, onFav = { photo ->
            photo?.let {
                homeViewModel.insertOrDelete(
                    Favorites(
                        it.url,
                        it.thumbnailUrl,
                        it.albumId ?: 0,
                        it.id ?: 0,
                        it.title,
                        !it.isFav
                    )
                )
            }
        })

        binding.rvPhotos.adapter = adapter
        homeViewModel.pagingData().observe(viewLifecycleOwner, {
            adapter.submitData(lifecycle, it)
        })

        return binding.root
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}