package com.myapi.apiexample.ui.filter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.myapi.apiexample.databinding.FragmentFilterBinding

class FilterFragment : Fragment() {

    private lateinit var filterViewModel: FilterViewModel
    private var _binding: FragmentFilterBinding? = null
    private val PICKER_MIN_VALUE = 0
    private val PICKER_MAX_VALUE = 5000
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        filterViewModel = ViewModelProvider(this).get(FilterViewModel::class.java)

        _binding = FragmentFilterBinding.inflate(inflater, container, false)
        binding.apply {
            lifecycleOwner = this@FilterFragment
            frag = this@FilterFragment
            viewModel = filterViewModel
        }
        binding.idPicker.maxValue = PICKER_MAX_VALUE
        binding.idPicker.minValue = PICKER_MIN_VALUE
        return binding.root
    }

    fun loadClick() {
        filterViewModel.loadPhoto(binding.idPicker.value)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}