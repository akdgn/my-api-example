package com.myapi.apiexample.ui.favorite

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.myapi.apiexample.repo.db.DbRepo

class FavoriteViewModel(private val app: Application) : AndroidViewModel(app) {

    private val dbRepo = DbRepo.instance(app.applicationContext)
    val favList = dbRepo.favs

}