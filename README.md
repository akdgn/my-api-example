# API EXAMPLE
By using jsonplaceholder API, the application gets the photos on the main page. There are 4 pages. 
1. page : The application is getting 50 images of 5000 in the first load. By scrolling, the application getting more. If the application can not connect to the internet, cached items will be shown.
2. page : Getting single image by id. By using the number picker, the single image data come out.
3. page : Post new data to API. By Filling blanked boxes, the application is posting a new item to API. 
4. page : Favorite list. By adding favorites from the main list, we are listing only favorite items. The application also shows which items are added to favorites in the main list.

### This project;
- is not tested by the unit test.
- is coded entirely Kotlin. Tried to use well-known libraries and components.

## The reasons for using libraries;
- Navigation Components : The navigation components provide us with that we can navigate, pass data easily, animations, etc... Thanks to navigation graph, the project structure can be understood very easily.
- Live Data : Thanks to the paging3 library, we can fetch the API data very easily and fast. In the past, it is used endless recyclerview for paging, and performance was so slow. By using the paging3 library, we can fetch our data faster. Also, it is caching the data while loading the new data. if there is any update in the list, the list automatically will be updated.
- By Using the mvvm structure, we will be coding more clean code and faster applications. When used data binding and live data, UI can be updated from the view model. So we don't need to update UI from fragment / Activity classes.
- Retrofit : This library is cool for REST API. Offline caching, usage, sending the request, updating UI issues are very easy with this library. 
- Room DB : In the past, as you know, it is coded two different classes for the SQL database. While the Room library solves many problems that we may not see, it offers an easy use with simple methods. I think this library is the best for local DB.
